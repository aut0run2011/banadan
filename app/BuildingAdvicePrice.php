<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BuildingTypePrice
 * @package App
 */
class BuildingAdvicePrice extends Model
{
    /**
     * @var string
     */
    protected $table = 'building_advice_price';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'building_type_ids','meter_start','meter_end','meter_price','position','is_active','advice_option_id','meter_off_price', 'id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function building(){
        return $this->belongsTo('App\BuildingType','building_type_id');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
/*    public function buildings(){
        return $this->belongsToMany('App\BuildingType','building_type_ids');
    }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advice(){
        return $this->belongsTo('App\AdviceOption','advice_option_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function apply()
    {
        return $this->belongsToMany('App\Apply', 'apply_building_advice_price', 'building_advice_price_id', 'apply_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function expert()
    {
        return $this->belongsToMany('App\User', 'apply_building_advice_price', 'building_advice_price_id', 'user_id')->withTimestamps();
    }
}
