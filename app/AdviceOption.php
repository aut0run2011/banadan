<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdviceOption
 * @package App
 */
class AdviceOption extends Model
{
    /**
     * @var string
     */
    protected $table = 'advice_option';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','parent','is_active','images','position'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('App\User', 'user_advice_option', 'advice_option_id', 'user_id')->withTimestamps();
    }
    /**
     * @param $value
     */
    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getImagesAttribute($value)
    {
        return is_array($value)?$value:json_decode($value,true);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function building_advice_price(){
        return $this->hasMany('App\BuildingAdvicePrice');
    }

}
