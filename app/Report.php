<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'apply_id','description','images', 'user_id'
  ];


  public function apply()
  {
      return $this->hasOne('App\Apply');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }
}
