<?php

namespace App\Http\Controllers;

use App\AdviceOption;
use App\Apply;
use App\Area;
use App\BuildingAdvicePrice;
use App\BuildingType;
use App\City;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ApplyController extends Controller
{
    public function create(){
        $data['buildingType']=tree(BuildingType::orderBy('position','ASC')->get()->toArray());
        $data['buildingAdvicePrice']=BuildingAdvicePrice::where("is_active",1)->get();
        $data['advice']=tree(AdviceOption::where("is_active",1)->get()->toArray());
        $data['state']=State::where("is_active",1)->get();
        $data['city']=City::where("is_active",1)->get();
        $data['area']=Area::where("is_active",1)->get();
        return $data;

    }

    public function store(Request $request){

        $validator=Validator::make($request->all(), [
            'area_id'    => 'required|required',
            'buildingType_id'=>'required|numeric',
            'advice_ids'=>'required|array',
            'advice_ids.*'=>'required|numeric',
            'phone'=>'required|numeric',
/*            'email'=>'required|email',
            'mobile'=>'required|size:11',*/
            'address'=>'required',

            'lat'=>'numeric',
            'lon'=>'numeric',
            'business_days'=>'required',
            'holidays'=>'required',
            'payment_type'=>['required',Rule::in(['online','place','report'])],
            "meter"=>"required|numeric",
            //'g_recaptcha_response' => 'required|recaptcha',
        ]);
        if ($validator->fails())
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        $input=$request->all();
        if(!$user=Auth::user()) {
            $user = User::where("mobile", "=", $input["mobile"])->first();
            if (!$user) {
                $password = strtolower(str_random(6));
                $user = User::create([
                    "email" => $input["email"],
                    "mobile" => $input["mobile"],
                    "password" => bcrypt($password),
                    "role" => "user",
                    "status" => "active"
                ]);
                sendSms($user->mobile,text_message_password($password));
                return response()->json(['status'=>'success',"message"=>["password"=>["رمز عبور توسط پیامک برا شما ارسال شد.",$password]]]);
            } else {
                if(!empty($input['password'])) {
                    Auth::attempt(['mobile' => $input["mobile"], 'password' => $input['password']]);
                    $user=Auth::user();
                    if (!$user)
                        return response()->json(['status' => 'error', "message" => ["password" => ["نام کاربری و یا رمز عبور اشتباه است"]]], 400);
                }else{
                    return response()->json(['status'=>'success',"message"=>["password"=>["شما قبلا در سیستم ثبت نام کرده اید لطفا رمز عبور خود را وارد کنید."]]]);
                }
            }
        }
        $apply = Apply::create([
            "user_id"       =>  $user->id,
            "area_id"       =>  $input['area_id'],
            "phone"         =>  $input['phone'],
            "mobile"        =>  $input["mobile"],
            "address"       =>  $input["address"],
            "context"       =>  $input["context"],
            "lat"           =>  $input["lat"],
            "lon"           =>  $input["lon"],
            "business_days" =>  $input["business_days"],
            "holidays"      =>  $input["holidays"],
            "payment_type"  =>  $input["payment_type"],
            "advice_option_ids"  =>  $input["advice_option_ids"],
            "advice_ids"  =>  $input["advice_ids"]

        ]);
        $BuildingAdvicePrice=BuildingAdvicePrice::where("building_type_id",$input['buildingType_id'])->whereIn("advice_option_id",$input['advice_ids']);
        $price=0;
        $off_price=0;
        if($input["meter"]>=1000){
            $BuildingAdvicePrice=$BuildingAdvicePrice->where("meter_start",$input['meter'])->get();
        }else{
            $BuildingAdvicePrice=$BuildingAdvicePrice->where("meter_start","<=",$input['meter'])
                ->where("meter_end",">=",$input['meter'])->get();
            foreach ($BuildingAdvicePrice as $key=>$val){
                $price += $val->meter_price * $input['meter'];
                $off_price += ($val->meter_price - (($val->meter_price / 100) * $val->meter_off_price)) * $input['meter'];
            }
        }
        $apply->price=$price;
        /*$apply->off_price=$off_price;*/
        $apply->save();
        $apply->building_advice_price()->sync($BuildingAdvicePrice->pluck("id"));

        return ["data"=>$apply];


    }

    public function price_check(Request $request){
        $validator=Validator::make($request->all(), [
            'buildingType_id'=>'required|numeric',
            'advice_ids'=>'required|array',
            'advice_ids.*'=>'required|numeric',
            "meter"=>"required|numeric"
        ]);
        if ($validator->fails())
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        $input=$request->all();
        $BuildingAdvicePrice=BuildingAdvicePrice::where("building_type_id",$input['buildingType_id'])->whereIn("advice_option_id",$input['advice_ids']);
        $price=0;
        $off_price=0;
        if($input["meter"]<=1000){
            $BuildingAdvicePrice=$BuildingAdvicePrice->where("meter_start","<=",$input['meter'])
                ->where("meter_end",">=",$input['meter'])->get();
            foreach ($BuildingAdvicePrice as $key=>$val){
                $price += $val->meter_price * $input['meter'];
                $off_price += ($val->meter_price - (($val->meter_price / 100) * $val->meter_off_price)) * $input['meter'];
            }
        }
        return ["data"=>["price"=>$price,"off_price"=>$off_price]];
    }
}
