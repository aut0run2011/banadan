<?php

namespace App\Http\Controllers;

use App\Area;
use App\User;
use App\City;
use App\Page;
use App\State;
use Illuminate\Http\Request;
use Kavenegar\KavenegarApi;
use Illuminate\Support\Facades\Validator;


/**
 * Class PublicController
 * @package App\Http\Controllers
 */
class PublicController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home(){

        return view('front.home.index');
    }

    public function test(){
      $user = User::find(5);
      return ['data' => $user->expert_apply()->get()];
    }


    public function page($slug)
    {
        $page=Page::Where('slug',$slug)->where('is_active',1)->first();
        if(empty($page))
            return redirect('/');
        return view('front.page.index',compact('page'));
    }

    /**
     * @return array
     */
     public function state(){
         return ["state"=>State::where("is_active",1)->get()];
     }




    /**
     * @param $state_id
     * @return array
     */
     public function city($state_id){
         $city=City::where("state_id",$state_id)->where("is_active",1)->get();
         return ["city"=>$city];
     }

    /**
     * @param $city_id
     * @return array
     */
    public function area($city_id){
        $area=Area::where("city_id",$city_id)->where("is_active",1)->get();
        return ["area"=>$area];
    }
}
