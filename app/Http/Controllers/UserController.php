<?php

namespace App\Http\Controllers;

use App\City;
use App\ShopCart;
use App\State;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use p3ym4n\JDate\JDate;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login(){
        \session(['referer'=>request()->headers->get('referer')]);
        return view('front.user.login');
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function auth(Request $request){

        $validator=Validator::make($request->all(), [
            //'g_recaptcha_response' => 'required|recaptcha',
            'email'=>'required',
            'password'=>'required',

        ]);
        if ($validator->fails())
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);

        $emailOrNationalCode = $request->get('email');
        $password = $request->get('password');
        $token = null;

        if(Auth::attempt(['email' => $emailOrNationalCode ,'password' => $password,'status'=>'active']) ||
            Auth::attempt(['mobile' => $emailOrNationalCode ,'password' => $password,'status'=>'active'])){
            $user=Auth::user();
            try {
                if (!$token = JWTAuth::attempt(['id' => $user->id ,'password' => $password,'status'=>'active'])){
                        return response()->json(['invalid_email_or_password'], 422);
                }
            } catch (JWTAuthException $e) {
                return response()->json(['failed_to_create_token'], 500);
            }

            return ['status'=>'success','data'=>["id" => $user->id,  "name"=>$user->name,"family"=>$user->family,"role"=>$user->role,"token"=>"Bearer {$token}",'referer'=>\session('referer')]];
        }else
            return response()->json(['status'=>'error',"message"=>["نام کاربری و یا رمز عبور اشتباه است"]],400);


    }

    /**
     *
     */
    public function register()
    {
        $data['city'] = City::all();
        $data['state'] = State::all();
        return view("front.user.register");
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request){

        $validator=Validator::make($request->all(), [
            'name' => 'required|regex:/^[0-9\pL\s\-]+$/u',
            'family' => 'required|regex:/^[0-9\pL\s\-]+$/u',
            'email' => 'required|email|unique:user,email',
            'mobile' => 'required|regex:/09[0-9]{9}/|unique:user,mobile',
            'password'=> 'min:6|required',
            'confirm_password'=>'required|same:password',
            'g-recaptcha-response' => 'required|recaptcha',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['type']='user';
        $input['remember_token']=str_random('32');
        $input['is_active']=1;
        $user = User::create($input);
        Mail::to($user)->send(new UserActive($user));
        return redirect()->back()->with('success', ['ثبت نام شما با موفقیت انجام شد.',
            'ایمیلی جهت فعال سازی اکانت به شما ارسال شد.']);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function logout() {
        Auth::logout();
        return redirect('/');
    }

    /**
     *
     */
    public function profile(){
        $user=Auth::user();
        $user->birthday=JDate::createFromTimestamp(strtotime($user->birthday))->format('Y-m-d');
        return view('front.user.profile',compact('user'));

    }


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request){
        $validator=Validator::make($request->all(), [
            'name'    => 'required|min:2|regex:/^[\pL\s\-]+$/u',
            'family'  => 'required|min:2|regex:/^[\pL\s\-]+$/u',
            'phone'   => 'required',
            'birthday'=> 'required',
            'gender'  => 'required',
            'national_code'=>'required|numeric'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $input = $request->all();
        $birthday=explode('-',$input['birthday']);
        $input['birthday'] = Carbon::createFromTimestamp(jmktime(0,0,0,$birthday[1],$birthday[2],$birthday[0]));
        Auth::user()->update([
            'name'=>$input['name'],
            'family'=>$input['family'],
            'phone'=>$input['phone'],
            'is_company'=>$input['is_company'],
            'company'=>$input['company'],
            'birthday'=>$input['birthday'],
            'gender'=>$input['gender'],
            'national_code'=>$input['national_code'],
        ]);

        return redirect()->back()->with('success', ['تغییرات با موفقیت انجام شد']);
    }

    /**
     * @param $id
     * @return string
     */
    public function delete_address($id){
        Auth::user()->address()->find($id)->delete();
        return 'Ok';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset_password(Request $request){
        $user=Auth::user();
        $validator=Validator::make($request->all(), [
            'password'=> 'min:6|required',
            'confirm_password'=>'required|same:password',
            'old_password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if(Hash::check($request->get('old_password'),$user->password)){
            $user->password = bcrypt($request->get('password'));
            $user->save();
            return back()->with(['success'=>["با موفقیت انجام شد."]]);
        }else{
            return back()->with(['success'=>["رمز عبور اشتباه است."]]);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reset_password_form(){
        return view('front.user.change_password');
    }

    /**
     * @param $token
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function password_recovery_store($token){
        if(strlen($token)!=32)
            abort(404);
        $user=User::where('remember_token',$token)->where('is_active',1)->first();
        if(!$user)
            abort(404);
        $validator=Validator::make(\request()->all(), [
            'password'=> 'min:6|required',
            'confirm_password'=>'required|same:password',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $user->password =bcrypt(\request()->get('password'));
        $user->remember_token ='';
        $user->save();
        Auth::loginUsingId($user->id);
        return redirect('/')->with(['success'=>['رمز عبور شما با موفقیت تغییر پیدا کرد.','شما لاگین شدین.']]);;
    }

    /**
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function password_recovery_create($token){
        return view('front.user.password_recovery',compact('token'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function forgot_password_create(){
        return view('front.user.forgot_password',compact('user'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function forgot_password_store(Request $request){
        $validator=Validator::make($request->all(), [
            'email' => 'required|email|exists:user,email',
            'g-recaptcha-response' => 'required|recaptcha',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $input=$request->all();
        $token=str_random(32);
        $user=User::where('email',$input['email'])->where('is_active',1)->first();
        if(isset($user->id)) {
            $user->update([
                'remember_token' => $token
            ]);
            Mail::to($user)->send(new ForgotPassword($user));
            return back()->with(['success' => ['ایمیلی جهت باز یابی رمز عبور برای شما ارسال شد.']]);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function address(){
        return view('front.user.address_list');
    }

    /**
     * @param $remember_token
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function active_email($remember_token){
        $user=User::Where('remember_token','=',$remember_token)->Where('active_email','=','0')->first();
        if($user){
            $user->active_email=1;
            $user->remember_token='';
            $user->save();
            Auth::loginUsingId($user->id);
            return redirect('/')->with(['success'=>['فعال سازی شما با موفقیت انجام شد.']]);
        }else{
            return redirect('/');
        }
    }



}
