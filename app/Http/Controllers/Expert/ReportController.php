<?php

namespace App\Http\Controllers\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Report;
use App\User;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $reports = new Report();
      $getFillable = getFillable($reports);
      foreach ($request->all() as $key => $val) {
          if (in_array($key, $getFillable))
              $reports = $reports->where($key, '=', $val);
      }
      return $reports->paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator=Validator::make($request->all(), [
          'apply_id' => 'required',
          'user_id' => 'required',
          'description' => 'required',
      ]);
      if ($validator->fails()) {
          return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
      }
      $input=$request->all();
      $report=Report::create($input);
      return ["action"=>'success','data'=>$report];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        return ['data'=>$report];
    }

    /**
     * Display all reports from specified user .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showUser(User $user)
    {
      $reports = $user->reports;
        return ['data'=>$reports];

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
      $validator=Validator::make($request->all(), [
          'apply_id' => 'required',
          'user_id' => 'required',
          'description' => 'required',
      ]);
      if ($validator->fails()) {
          return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
      }
      $input=$request->all();
      $report->update($input);
      return ["action"=>'success','data'=>$report];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
