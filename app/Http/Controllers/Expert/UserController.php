<?php

namespace App\Http\Controllers\Expert;

use App\AdviceOption;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
     {
         $user=new User();
         $getFillable=getFillable($user);
         foreach($request->all() as $key=>$val){
             if (in_array($key,$getFillable))
                 $user=$user->where($key,'=',$val);
         }
         return $user->with('advice_option')->where('role', 'expert')->paginate();

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(User $user)
     {
         $data['data']['user']=$user;
         $data['data']['user_advice_option']=$user->advice_option()->get();
         return $data;
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(User $user)
     {

         $data['data']['gender']=[['type'=>'male','text'=>'مرد'],['type'=>'female','text'=>'زن']];
         $data['data']['role']=[['type'=>'user','text'=>'کاربر'],['type'=>'expert','text'=>'کارشناس'],['type'=>'admin','text'=>'ادمین']];
         $data['data']['user']=$user;
         $data['data']['user_advice_option']=$user->advice_option()->get();
         $data['data']['advice_option']=tree(AdviceOption::where("is_active",1)->get()->toArray());
         return $data;
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, User $user)
     {

         $validator=Validator::make($request->all(), [
             'name' => 'min:2',
             'family' => 'min:2',
             //'email' => 'email|unique:user,email',
             //'mobile' => 'unique:user,mobile|min:11|max:11',
             'gender' =>[Rule::in(['male','female'])],
             'password'=> 'min:6',
             'role' =>['required',Rule::in(['user','expert','admin'])],
             'status' =>['required',Rule::in(['active','inactive','disabled_by_admin'])],
             'national_code'=>'size:10',
             'postal_code'=>'size:10',
             'birthday'=>'required',
             "images" => 'array',
             "advice_option_ids"=>'array',
             "advice_option_ids.*"=>'numeric'

         ]);
         if ($validator->fails()) {
             return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
         }
         $input=$request->only(['name','family','gender','password','role','status','national_code','postal_code','birthday',"images","advice_option_ids"]);
         if (isset($input['password']))$input['password']=bcrypt($input['password']);
         $user->update($input);
         if(isset($input['advice_option_ids']))
             $user->advice_option()->sync($input['advice_option_ids']);
         return ["action"=>'success','data'=>$user];
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
