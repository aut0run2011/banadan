<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MenuItem;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class MenuItemController
 * @package App\Http\Controllers\Admin
 */
class MenuItemController extends Controller
{

    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $MenuItem=tree(MenuItem::where('menu_id','=',$request->get('menu_id'))->orderBy('position','ASC')->get()->toArray());
        return ['data'=>$MenuItem];
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'menu_id' => 'required|numeric',
            'title' => 'required|min:2',
            'link' => 'required',
            'position' => 'required|numeric',
            'parent' => 'numeric',

        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $user=MenuItem::create($input);
        return ["action"=>'success','data'=>$user];
    }


    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $data['data']=MenuItem::find($id);
        return $data;
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request){
        $array=tree_to_flatten($request->get('data'));

        foreach ($array as $key=>$val) {
            $validator=Validator::make($val, [
                'menu_id' => 'required|numeric',
                'title' => 'required|min:2',
                'link' => 'required',
                'parent' => 'numeric',

            ]);
            if ($validator->fails()) {
                $return[$key]=['status' => 'error', "message" => $validator->errors()];
            }
            $val['position']=$key;
            if(empty($val['id'])) {
                $menuItem = MenuItem::create($val);
            }else{
                $menuItem=MenuItem::find($val['id']);
                if(is_array($menuItem->images)) {
                    delete_discord_image($menuItem->images, $val['images']);

                }

                $menuItem->update($val);
            }
            $return[$key]=["action" => 'success', 'data' => $menuItem];
        }
        return $return;
    }


    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status($id){
        $validator=Validator::make(\request()->all(), [
            'is_active'   =>['required',Rule::in(['0','1'])]
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $menuItem=MenuItem::find($id);
        $menuItem->update(['is_active'=>\request()->get('is_active')]);
        return ["action"=>'success','data'=>$menuItem];
    }
}
