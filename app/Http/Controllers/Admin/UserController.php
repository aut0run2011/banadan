<?php

namespace App\Http\Controllers\Admin;

use App\AdviceOption;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin
 */
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user=new User();
        $getFillable=getFillable($user);
        foreach($request->all() as $key=>$val){
            if (in_array($key,$getFillable))
                $user=$user->where($key,'=',$val);
        }
        return $user->with('advice_option')->paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['data']['gender']=[['type'=>'male','text'=>'مرد'],['type'=>'female','text'=>'زن']];
        $data['data']['role']=[['type'=>'user','text'=>'کاربر'],['type'=>'expert','text'=>'کارشناس'],['type'=>'admin','text'=>'ادمین']];
        $data['data']['advice_option']=tree(AdviceOption::where("is_active",1)->get()->toArray());
        return $data;
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //var_dump($request->all());die;
        $validator=Validator::make($request->all(), [
            'name' => 'min:2',
            'family' => 'min:2',
            'email' => 'email|unique:user,email',
            'mobile' => 'unique:user,mobile|size:11',
            'gender' =>[Rule::in(['male','female'])],
            'password'=> 'min:6|required',
            'role' =>['required',Rule::in(['user','expert','admin'])],
            'status' =>['required',Rule::in(['active','inactive','disabled_by_admin'])],
            'national_code'=>'size:10',
            'postal_code'=>'size:10',
            'birthday'=>'required',
            "images" => 'array',
            "advice_option_ids"=>'array',
            "advice_option_ids.*"=>'numeric'

        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        if (isset($input['password']))$input['password']=bcrypt($input['password']);
        $user=User::create($input);
        if(isset($input['advice_option_ids']))
            $user->advice_option()->sync($input['advice_option_ids']);
        return ["action"=>'success','data'=>$user];

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $data['data']['user']=$user;
        $data['data']['user_advice_option']=$user->advice_option()->get();
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $data['data']['gender']=[['type'=>'male','text'=>'مرد'],['type'=>'female','text'=>'زن']];
        $data['data']['role']=[['type'=>'user','text'=>'کاربر'],['type'=>'expert','text'=>'کارشناس'],['type'=>'admin','text'=>'ادمین']];
        $data['data']['user']=$user;
        $data['data']['user_advice_option']=$user->advice_option()->get();
        $data['data']['advice_option']=tree(AdviceOption::where("is_active",1)->get()->toArray());
        return $data;
    }


    /**
     * @param Request $request
     * @param User $user
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {

        $validator=Validator::make($request->all(), [
            'name' => 'min:2',
            'family' => 'min:2',
            //'email' => 'email|unique:user,email',
            //'mobile' => 'unique:user,mobile|min:11|max:11',
            'gender' =>[Rule::in(['male','female'])],
            'password'=> 'min:6',
            'role' =>['required',Rule::in(['user','expert','admin'])],
            'status' =>['required',Rule::in(['active','inactive','disabled_by_admin'])],
            'national_code'=>'size:10',
            'postal_code'=>'size:10',
            'birthday'=>'required',
            "images" => 'array',
            "advice_option_ids"=>'array',
            "advice_option_ids.*"=>'numeric'

        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->only(['name','family','gender','password','role','status','national_code','postal_code','birthday',"images","advice_option_ids"]);
        if (isset($input['password']))$input['password']=bcrypt($input['password']);
        $user->update($input);
        if(isset($input['advice_option_ids']))
            $user->advice_option()->sync($input['advice_option_ids']);
        return ["action"=>'success','data'=>$user];
    }

    /**
     * @param User $user
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(User $user){
        $validator=Validator::make(request()->all(), [
            'status' =>['required',Rule::in(['by_admin','disabled_by_admin'])],
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $user->update(['is_active'=>request()->get('status')]);
        return ["action"=>'success','data'=>$user];
    }
}
