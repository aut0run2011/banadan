<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class PageController
 * @package App\Http\Controllers\Admin
 */
class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page=new Page();
        $getFillable=getFillable($page);
        foreach($request->all() as $key=>$val){
            if (in_array($key,$getFillable))
                $page=$page->where($key,'=',$val);
        }
        return $page->paginate();
    }


    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'slug'    => 'required|min:3|unique:page',
            'title'=>'required',
            'is_active'   =>['required',Rule::in(['0','1'])]
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $page=Page::create($input);
        return ["action"=>'success','data'=>$page];
    }


    /**
     * @param $id
     * @return array
     */
    public function show($id)
    {
        $page=Page::find($id);
        return ['data'=>$page];
    }


    /**
     * @param Request $request
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(), [
            'slug'    => 'required|min:3|unique:page',
            'title'=>'required',
            'is_active'   =>['required',Rule::in(['0','1'])]
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $page=Page::find($id);
        $page=$page->update($input);
        return ["action"=>'success','data'=>$page];
    }

    /**
     * @param Page $page
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(Page $page){
        $validator=Validator::make(\request()->all(), [
            'is_active'   =>['required',Rule::in(['0','1'])]
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $page->update(['is_active'=>\request()->get('is_active')]);
        return ["action"=>'success','data'=>$page];
    }
}
