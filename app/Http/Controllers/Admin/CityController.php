<?php

namespace App\Http\Controllers\Admin;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

/**
 * Class CityController
 * @package App\Http\Controllers\Admin
 */
class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $city = new City();
        $getFillable = getFillable($city);
        foreach ($request->all() as $key => $val) {
            if (in_array($key, $getFillable))
                $city = $city->where($key, '=', $val);
        }
        return $city->paginate();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|min:2',
            'state_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $city=City::create($input);
        return ["action"=>'success','data'=>$city];
    }



    /**
     * @param City $city
     * @return array
     */
    public function show(City $city)
    {
        return ['data'=>$city];
    }

    /**
     * @param Request $request
     * @param City $city
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, City $city)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|min:2',
            'state_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $city->update($input);
        return ["action"=>'success','data'=>$city];
    }

    /**
     * @param City $city
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(City $city){
        $validator=Validator::make(request()->all(), [
            'is_active' =>['required',Rule::in(['0','1'])],
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $city->update(['is_active'=>request()->get('is_active')]);
        return ["action"=>'success','data'=>$city];
    }
}
