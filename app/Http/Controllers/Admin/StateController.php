<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use App\City;
use App\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

/**
 * Class StateController
 * @package App\Http\Controllers\Admin
 */
class StateController extends Controller
{

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $state = new State();
        $getFillable = getFillable($state);
        foreach ($request->all() as $key => $val) {
            if (in_array($key, $getFillable))
                $state = $state->where($key, '=', $val);
        }
        return $state->paginate();
    }


    /**
     *
     */
    public function create()
    {
        //
    }


    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|min:2',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $auction=State::create($input);
        return ["action"=>'success','data'=>$auction];
    }


    /**
     * @param State $state
     * @return array
     */
    public function show(State $state)
    {
        return ['data'=>$state];
    }

    /**
     * @param Request $request
     * @param State $state
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, State $state)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|min:2',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $state->update($input);
        return ["action"=>'success','data'=>$state];
    }


    /**
     * @param State $state
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(State $state){
        $validator=Validator::make(request()->all(), [
            'is_active' =>['required',Rule::in(['0','1'])],
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $state->update(['is_active'=>request()->get('is_active')]);
        return ["action"=>'success','data'=>$state];
    }

    /**
     * @return array
     */
    public function state(){
        return ["state"=>State::all()];
    }

    /**
     * @param $state_id
     * @return array
     */
    public function city($state_id){
        $city=City::where("state_id",$state_id)->get();
        return ["city"=>$city];
    }

    /**
     * @param $city_id
     * @return array
     */
    public function area($city_id){
        $area=Area::where("city_id",$city_id)->get();
        return ["area"=>$area];
    }
}
