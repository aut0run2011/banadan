<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

/**
 * Class MenuController
 * @package App\Http\Controllers\Admin
 */
class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $menu=new Menu();
        $getFillable=getFillable($menu);
        foreach($request->all() as $key=>$val){
            if (in_array($key,$getFillable))
                $menu=$menu->where($key,'=',$val);
        }
        return $menu->paginate();
    }


    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'slug' => 'required|min:2',
            'title' => 'required|min:2',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $user=Menu::create($input);
        return ["action"=>'success','data'=>$user];
    }


    /**
     * @param $id
     * @return array
     */
    public function show($id)
    {
        $data['data']=Menu::find($id);
        return ['data'=>$data];
    }


    /**
     * @param Request $request
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(), [
            'slug' => 'required|min:2',
            'title' => 'required|min:2',

        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $menu=Menu::find($id);
        $menu->update($input);
        return ["action"=>'success','data'=>$menu];
    }


    /**
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        $menu=Menu::find($id);
        $menu->update(['is_active'=>0]);
        return ["action"=>'success'];
    }

    /**
     * @param Menu $menu
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(Menu $menu){
        $validator=Validator::make(\request()->all(), [
            'is_active'   =>['required',Rule::in(['0','1'])]
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $menu->update(['is_active'=>\request()->get('is_active')]);
        return ["action"=>'success','data'=>$menu];
    }
}
