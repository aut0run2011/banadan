<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

/**
 * Class AreaController
 * @package App\Http\Controllers\Admin
 */
class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $area = new Area();
        $getFillable = getFillable($area);
        foreach ($request->all() as $key => $val) {
            if (in_array($key, $getFillable))
                $area = $area->where($key, '=', $val);
        }
        return $area->paginate();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|min:2',
            'region' => 'required',
            'city_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $area=Area::create($input);
        return ["action"=>'success','data'=>$area];
    }


    /**
     * @param Area $area
     * @return array
     */
    public function show(Area $area)
    {
        $area->city->state;
        return ['data'=>["area"=>$area]];
    }

    /**
     * @param Request $request
     * @param Area $area
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Area $area)
    {
        $validator=Validator::make($request->all(), [
            'name' => 'required|min:2',
            'region' => 'required',
            'city_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $area->update($input);
        return ["action"=>'success','data'=>$area];
    }


    /**
     * @param Area $area
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(Area $area){
        $validator=Validator::make(request()->all(), [
            'is_active' =>['required',Rule::in(['0','1'])],
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $area->update(['is_active'=>request()->get('is_active')]);
        return ["action"=>'success','data'=>$area];
    }
}
