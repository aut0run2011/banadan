<?php

namespace App\Http\Controllers\Admin;

use App\Attachment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

/**
 * Class AttachmentController
 * @package App\Http\Controllers\Admin
 */
class AttachmentController extends Controller
{
    /**
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(){
        set_time_limit(0); 
        $date=date('mdY');
        $origin_file=base_path('html/uploads/origin/'.$date);
        $thumbnail=base_path('html/uploads/thumbnail/'.$date);
        if(!File::exists('uploads/origin')) {
            File::makeDirectory('uploads/origin',0777);
            File::makeDirectory('uploads/thumbnail',0777);
        }
        if(!File::exists($origin_file)) {
            File::makeDirectory($origin_file,0777);
        }
        if(!File::exists($thumbnail)) {
            File::makeDirectory($thumbnail,0777);
        }
        $file_type_array=[
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg'=> 'image/jpeg',
            'jpg' => 'image/jpg'
        ];
        $file=Input::file('photo');
        $mime = $file->getMimeType();
        $images=[];
        if(array_search($mime,$file_type_array)){
            $fileName=time().'_'.str_replace(' ','-',$file->getClientOriginalName());
            $file->move($origin_file, $fileName);
            foreach (Input::get('config') as $key=>$size){
                $img=Image::make($origin_file.'/'.$fileName);
                list($width,$height)=explode('/',$size);
                $file_name_1="{$width}*{$height}-".$fileName;
                $img->resize($width,$height);
                //if(Input::get('watermark')=='1') {
                    //$thumbnail_size=($width/100)*20;
                    //$watermark=Image::make(base_path('html/uploads/watermark.png'))->resize($thumbnail_size,$thumbnail_size);
                    //$img->insert($watermark, 'top-left', 5, 5);
                //}
                $img->save($thumbnail.'/'.$file_name_1);
                $attachment=Attachment::create([
                    'path' => 'uploads/thumbnail/'.$date,
                    'mime' => $mime,
                    'file' => $file_name_1,
                    'size' => intval($img->filesize()/1024),
                 ]);
                $images[$key]['id'] =$attachment->id;
                $images[$key]['url']='uploads/thumbnail/'.$date.'/'.$file_name_1;
                $images[$key]['size']=$size;
                $images[$key]['image_id']=$attachment->id;
            }
            return ['data'=>$images];
        }else{
            return response()->json(['status'=>'error',"message"=>'فایل ارسالی مجاز نیست.'],400);
        }

    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request){
        $validator=Validator::make($request->all(), [
            'image_ids'=>'array',
            'image_ids.*'=>'numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $attach=Attachment::whereIn('id',$request->get('image_ids'))->get();
        foreach ($attach as $val){
            unlink(base_path("html/{$val->path}/{$val->file}"));
            $val->delete();
        }

        if(isset($attach[0]->file)) {
            $ex = explode('-', $attach[0]->file);
            $ex2= explode('/', $attach[0]->path);
            $origin_file = str_replace("{$ex[0]}-", '', $attach[0]->file);
            unlink(base_path("html/uploads/origin/{$ex2[count($ex2)-1]}/{$origin_file}"));
        }

        return ['status'=>'success'];
    }
}
