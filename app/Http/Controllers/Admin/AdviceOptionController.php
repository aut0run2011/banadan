<?php

namespace App\Http\Controllers\Admin;

use App\AdviceOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class AdviceOptionController
 * @package App\Http\Controllers\Admin
 */
class AdviceOptionController extends Controller
{

    /**
     * @return array
     */
    public function index()
    {
        $AdviceOption=tree(AdviceOption::orderBy('position','ASC')->get()->toArray());
        return ['data'=>$AdviceOption];
    }


    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validator=Validator::make($request->all(), [
            'name'    => 'required|min:2',
            'parent'=>'required|numeric',
            'images'=>'array',
            'images.*.image_id'=>'numeric'
        ]);
        if ($validator->fails())
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);

        $input=$request->all();
        $input['position']=0;
        $AdviceOption=AdviceOption::create($input);
        return ["action"=>'success','data'=>$AdviceOption];
    }


    /**
     * @param AdviceOption $adviceOption
     * @return array
     */
    public function show(AdviceOption $adviceOption)
    {
        return ['data' => $adviceOption];
    }


    /**
     * @return mixed
     */
    public function update()
    {
        $array=tree_to_flatten(request()->get('data'));

        foreach ($array as $key=>$val) {
            $validator = Validator::make($val, [
                'id'=> 'numeric',
                'name' => 'required|min:2',
                'parent' => 'required',
                'is_active' => ['required', Rule::in(['0', '1'])],
                'images'=>'array',
                'images.*.image_id'=>'numeric'
            ]);
            if ($validator->fails()) {
                $return[$key]=['status' => 'error', "message" => $validator->errors()];
            }
            $val['position']=$key;
            if(empty($val['id'])) {
                $cat = AdviceOption::create($val);
            }else{
                $cat=AdviceOption::find($val['id']);
                $cat->update($val);
            }
            $return[$key]=["action" => 'success', 'data' => $cat];
        }
        return $return;
    }


    /**
     * @param AdviceOption $adviceOption
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(AdviceOption $adviceOption){
        $validator=Validator::make(request()->all(), [
            'is_active' =>['required',Rule::in(['0','1'])],
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $adviceOption->update(['is_active'=>request()->get('status')]);
        return ["action"=>'success','data'=>$adviceOption];
    }
}
