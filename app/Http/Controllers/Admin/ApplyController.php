<?php

namespace App\Http\Controllers\Admin;

use App\AdviceOption;
use App\Apply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\User;

/**
 * Class ApplyController
 * @package App\Http\Controllers\Admin
 */
class ApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apply = new Apply();
        $getFillable = getFillable($apply);
        foreach ($request->all() as $key => $val) {
            if (in_array($key, $getFillable))
                $apply = $apply->where($key, '=', $val);
        }
        return $apply->paginate();
    }


    /**
     * @param Apply $apply
     * @return array
     */
    public function show(Apply $apply)
    {
        $apply->area->city->state;
        $data['apply']=$apply;
        $data['user']=$apply->user;
        $data['advice_option']=$apply->building_advice_price()->with('advice')->with('building')->get();
        $data['expert']=$apply->expert()->get();
        if ($apply->advice_ids === null) {
          $apply->advice_ids = [];
        }
        $data['advice_options'] = AdviceOption::whereIn('id', $apply->advice_ids)->with('user')->get();

        /*$data['suitable_experts'] = User::where('role', 'expert')->whereIn*/
        return ['data'=>$data];
    }


    /**
     * @param Apply $apply
     * @return array
     */
    public function edit(Apply $apply)
    {

        $apply->area->city->state;
        $apply->user;
        $data['apply']=$apply;
        $advice_option=$apply->building_advice_price()->with('advice')->with('building')->get();
        $data['expert']=$apply->expert()->get();
        $data['all_expert']=AdviceOption::whereIn('id',$advice_option->pluck('advice_option_id'))->with(['user'=>function ($query){
            $query->where('status','active');
        }])->get();
        $data['advice_option']=$advice_option;
        return ['data'=>$data];
    }


    /**
     * @param Request $request
     * @param Apply $apply
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Apply $apply)
    {
        $validator=Validator::make($request->all(), [
            'is_payment'    => ['required',Rule::in(['0','1'])],
            'expert_advice'    => 'array',
            'expert_advice.*.advice_option_id'    => 'numeric',
            'expert_advice.*.user_id'    => 'numeric'
        ]);
        if ($validator->fails())
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);

        $input=$request->only(['is_payment','expert_advice']);
        $apply=$apply->update($input);

        foreach ($input['expert_advice'] as $val)
            DB::table('apply_building_advice_price')
                ->where('apply_id',$apply->id)
                ->where('building_advice_price_id',$val['building_advice_price_id'])
                ->update(['user_id' => $val['user_id']]);
        return ["action"=>'success','data'=>['apply'=>$apply]];
    }
}
