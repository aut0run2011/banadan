<?php

namespace App\Http\Controllers\Admin;

use App\BuildingType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class BuildingTypeController
 * @package App\Http\Controllers\Admin
 */
class BuildingTypeController extends Controller
{

    /**
     * @return array
     */
    public function index()
    {
        $BuildingType=tree(BuildingType::orderBy('position','ASC')->get()->toArray());
        return ['data'=>$BuildingType];
    }


    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name'    => 'required|min:2',
            'parent'=>'required|numeric',
            'images'=>'array',
            'images.*.image_id'=>'numeric'
        ]);
        if ($validator->fails())
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);

        $input=$request->all();
        $input['position']=0;
        $BuildingType=BuildingType::create($input);
        return ["action"=>'success','data'=>$BuildingType];
    }


    /**
     * @param BuildingType $buildingType
     * @return array
     */
    public function show(BuildingType $buildingType)
    {
        return ['data' => $buildingType];
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        $array=tree_to_flatten($request->get('data'));

        foreach ($array as $key=>$val) {
            $validator = Validator::make($val, [
                'id'=> 'numeric',
                'name' => 'required|min:2',
                'parent' => 'required',
                'is_active' => ['required', Rule::in(['0', '1'])],
                'images'=>'array',
                'images.*.image_id'=>'numeric'
            ]);
            if ($validator->fails()) {
                $return[$key]=['status' => 'error', "message" => $validator->errors()];
            }
            $val['position']=$key;
            if(empty($val['id'])) {
                $cat = BuildingType::create($val);
            }else{
                $cat=BuildingType::find($val['id']);
                $cat->update($val);
            }
            $return[$key]=["action" => 'success', 'data' => $cat];
        }
        return $return;
    }


    /**
     * @param BuildingType $buildingType
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(BuildingType $buildingType){
        $validator=Validator::make(request()->all(), [
            'status' =>['required',Rule::in(['by_admin','disabled_by_admin'])],
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $buildingType->update(['is_active'=>request()->get('status')]);
        return ["action"=>'success','data'=>$buildingType];
    }

}
