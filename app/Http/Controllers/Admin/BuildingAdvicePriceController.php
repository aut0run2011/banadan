<?php

namespace App\Http\Controllers\Admin;

use App\AdviceOption;
use App\BuildingAdvicePrice;
use App\BuildingType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class MenuItemController
 * @package App\Http\Controllers\Admin
 */
class BuildingAdvicePriceController extends Controller
{

    /**
     * @param Request $request
     * @return array
     */
    public function index()
    {
        $BuildingAdvicePrice=BuildingAdvicePrice::orderBy('position','ASC')->get();
        return ['data'=>$BuildingAdvicePrice];
    }
    public function create(){
        return ["data"=>[
            'advice_option'=>tree(AdviceOption::where("is_active",1)->get()->toArray()),
            "building_type"=>tree(BuildingType::where("is_active",1)->get()->toArray())
        ]];
    }
    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'building_type_id' => 'required|numeric',
            'meter_start' => 'required|numeric',
            'meter_end' => 'required|numeric',
            'meter_price' => 'required|numeric',
            'meter_off_price' => 'required',
            'is_active' => 'numeric',

        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $input=$request->all();
        $input['position']=0;
        $BuildingAdvicePrice=BuildingAdvicePrice::create($input);
        return ["action"=>'success','data'=>$BuildingAdvicePrice];
    }


    /**
     * @param BuildingAdvicePrice $BuildingAdvicePrice
     * @return array
     */
    public function show(BuildingAdvicePrice $BuildingAdvicePrice)
    {
        return ['data'=>$BuildingAdvicePrice];
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request,BuildingAdvicePrice $buildingAdvicePrice ){
        $validator=Validator::make($request->all(), [
                'building_type_id' => 'required|numeric',
                'meter_start' => 'required|numeric',
                'meter_end' => 'required|numeric',
                'meter_price' => 'required|numeric',
                'meter_off_price' => 'required',
                'is_active' => 'numeric',

            ]);
            if ($validator->fails()) {
                return ['status' => 'error', "message" => $validator->errors()];
            }
            $input=$request->all();
            $input['position']=0;
            $buildingAdvicePrice->update($input);
            return ["action"=>'success','data'=>$buildingAdvicePrice];
    }


    /**
     * @param BuildingAdvicePrice $BuildingAdvicePrice
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function status(BuildingAdvicePrice $BuildingAdvicePrice){
        $validator=Validator::make(request()->all(), [
            'status' =>['required',Rule::in(['by_admin','disabled_by_admin'])],
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'error',"message"=>$validator->errors()],400);
        }
        $BuildingAdvicePrice->update(['is_active'=>request()->get('status')]);
        return ["action"=>'success','data'=>$BuildingAdvicePrice];
    }
}
