<?php

namespace App\Http\Controllers;

use App\Apply;
use App\Transaction;

use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function pay($id){
        $user=Auth::user();
        $hash_str=str_random(32);
        $apply=Apply::where('is_payment','=',0)->where('id',$id)->first();
        if($apply->price==0)
            return redirect("/?status=canceled");
        if(empty($apply)){
            return back()->with('success',['این فاکتور موجود نیست .']);
        }else {
            $apply->transaction()->create([
                'ip' => request()->ip(),
                'user_agent' => request()->server('HTTP_USER_AGENT'),
                'hash_str' => $hash_str
            ]);
            zarinpal_pay($apply->off_price, url("pay/verify/{$hash_str}"), 'ثبت سفارش بنادان', $user->email, $user->mobile);
        }
    }

    /**
     * @param $hash_str
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function verify($hash_str){
        $transaction=Transaction::where('hash_str',$hash_str)->first();
        if (empty($transaction))
            return redirect('/');
        $apply=$transaction->apply()->first();
        $verify=zarinpal_verify($apply->off_price);
        if ($verify['status']=='success'){
            $apply->is_payment = '1';
            $apply->save();
            $transaction->status_pay = $verify['status'];
            $transaction->RefID      = $verify['RefID'];
            $transaction->save();
            return redirect("/?status={$verify['status']}");
            //return view('front.order.factor',compact('payment'));
        }else{
            $transaction->status_pay = $verify['status'];
            $transaction->save();
            return redirect("/?status={$verify['status']}");
            //return view('front.order.factor_error',compact('payment'));
        }

    }
}
