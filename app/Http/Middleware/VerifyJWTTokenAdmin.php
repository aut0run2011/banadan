<?php
namespace App\Http\Middleware;
use Closure;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class VerifyJWTTokenAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (! $request->header('Authorization')) {
            return response()->json(['error'=>'لاگین کنید.'],401);
        }
        try {

            $user = JWTAuth::parseToken($request->header('Authorization'))->authenticate();
            if($user->role!='admin')
                return response()->json(['status' => 'error', 'messages' => ['Permission Denied']], 403);

        } catch (JWTException $e) {
            if ($e instanceof TokenInvalidException){
                return response()->json(['error'=>'Token is Invalid'],499);
            }else if ($e instanceof TokenExpiredException){
                return response()->json(['error'=>'Token is Expired'],498);
            }else{
                return response()->json(['error'=>'Something is wrong'],498);
            }
        }
        return $next($request);
    }
}