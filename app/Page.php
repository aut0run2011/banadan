<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Page
 * @package App
 */
class Page extends Model
{
    /**
     * @var string
     */
    protected $table = 'page';

    public $autoincrement = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'slug','title','description','content','is_active','images'
    ];

    public function setContentAttribute($value){
        $this->attributes['content'] = e($value);
    }

    /**
     * @param $value
     */
    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getImagesAttribute($value)
    {
        return is_array($value)?$value:json_decode($value,true);
    }
}
