<?php
    /**
     * @param $string
     * @param bool $return_data
     * @return bool|mixed
     */
    function is_json($string, $return_data = false) {
        $data = json_decode($string,TRUE);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

    /**
     * @param $array
     * @return mixed
     */
    function getRoutes($array)
    {
        foreach (\Illuminate\Support\Facades\Route::getRoutes() as $key => $val) {
            //dd($val->getName());
            foreach ($array as $var)
                $name[$var][$key] = $val->$var();
        }
        return $name;
    }

    /**
     * @param $val
     * @param $method
     * @return string
     */
    function get_rout_name($val, $method){
        $val=str_replace(url('/').'/','',$val);
        return app('router')->getRoutes()->match(app('request')->create($val, $method))->getName();
    }

    /**
     * @param $object
     * @return array
     */
    function getFillable($object)
    {
        return array_merge(['id'],$object->getFillable());
    }

    /**
     * @param array $elements
     * @param int $parentId
     * @return array
     */
    function tree(array $elements, $parentId = 0) {
        $branch = array();
        foreach ($elements as $element) {
            if($element['id'] != 0)
                if ($element['parent'] == $parentId) {

                    $children = tree($elements, $element['id']);
                    if ($children) {
                        $element['children'] = $children;
                    }else{
                        $element['children']=[];
                    }
                    $branch[] = $element;
                }
        }
        return $branch;
    }

    /**
     * @param $array
     * @return mixed
     */
    function tree_to_flatten($array) {
        global $result;
        global $flatten_index;
        if (!is_array($array)) {
            return false;
        }
        if(!isset($result))
            $result = [];
        if(!isset($flatten_index))
            $flatten_index = 0;

        foreach ($array as $key => $value) {

            if ($key!=="images" && is_array($value)) {

                if(is_int($key))
                    $flatten_index++;
                tree_to_flatten($value);
            } else {
                $result[$flatten_index][$key] = $value;

            }
        }
        return $result;

    }

    /**
     * @param $array_1
     * @param $array_2
     * @param string $key
     * @return array
     */
    function array_discord($array_1, $array_2, $key='image_id'){
        $column=array_column($array_2, $key);
        $return=[];
        foreach ($array_1 as $val){
            foreach($val as $val2)
                if(isset($val2[$key]) && !in_array($val2[$key],$column))
                    $return[]=$val;
        }
        return $return;
    }

    /**
     * @param $array_1
     * @param $array_2
     * @param string $key
     * @return bool
     */
    function delete_discord_image($array_1, $array_2, $key='image_id'){
        $discord=array_discord($array_1,$array_2,$key);
        if(count($discord)>0) {
            $discord=collect($discord);
            $discord_id = $discord->pluck('image_id')->toArray();
            $discord_url = $discord->pluck('url')->toArray();
            Illuminate\Support\Facades\DB::table('attachment')->whereIn('id', $discord_id)->delete();
            foreach ($discord_url as $val)
                Illuminate\Support\Facades\File::delete(base_path('public_html/' . $val));
            return true;
        }else{
            return false;
        }

    }

/**
 * @param $data
 * @param null $type
 * @param null $size
 * @return array|\Illuminate\Contracts\Routing\UrlGenerator|string
 */
    function image_link($data, $type=NULL, $size=NUll){

        if (!is_array($data)) {
            $data = json_decode($data, true);
        }
        $links=[];
        if(!empty($type) && !empty($size)) {
            foreach ($data as $key=>$val) {
                foreach ($val as $item) {
                    if ($item['type'] == $type && $item['size'] == $size) {
                        return url($item['url']);
                    }
                }
            }
        }elseif (!empty($type)){
            foreach ($data as $key=>$val) {
                foreach ($val as $item) {
                    if ($item['type'] == $type ) {
                        $links[]=url($item['url']);
                    }
                }
            }
            return $links;
        }elseif (!empty($size)){
            foreach ($data as $key=>$val) {
                foreach ($val as $item) {
                    if ($item['size'] == $size) {
                        $links[]=url($item['url']);
                    }
                }
            }
            return $links;
        }

        return url('uploads/watermark.png');

    }

/**
 * @param $Amount
 * @param $CallbackURL
 * @param $Description
 * @param $Email
 * @param $Mobile
 */
function zarinpal_pay($Amount, $CallbackURL, $Description, $Email, $Mobile)
    {
        $MerchantID = 'fb7fa010-f054-11e7-a7c4-000c295eb8fc'; //Required
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $client->PaymentRequest(
            [
                'MerchantID' => $MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $Email,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );

//Redirect to URL You can do it also by creating a form
        if ($result->Status == 100) {
            Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority);
//برای استفاده از زرین گیت باید ادرس به صورت زیر تغییر کند:
//Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority.'/ZarinGate');
        } else {
            echo'ERR: '.$result->Status;
        }
    }

    /**
     * Zarinpal verify
     */
    function zarinpal_verify($Amount)
    {
        $MerchantID = '17923966-a80c-11e7-91fc-000c295eb8fc';
        $Authority = $_GET['Authority'];

        if ($_GET['Status'] == 'OK') {

            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ]
            );

            if ($result->Status == 100) {
                return ['status'=>'success','RefID'=>$result->RefID];
            } else {
                return ['status'=>$result->Status];
            }
        } else {
            return ['status'=>'canceled'];
        }
    }

    function sendSms($receptor,$message){
        $sender = "10000077000707";
        $api = new \Kavenegar\KavenegarApi("6C4978327437456262724D695179776F682F442F4A4B596E2B4E355547664974");
        $api->Send($sender, $receptor, $message);
    }

    function text_message_password($password){
        return "ثبت نام شما با موفقیت انجام شد رمز عبور شما:{$password}
        http://banadan.com";
    }
