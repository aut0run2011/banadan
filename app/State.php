<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class State
 * @package App
 */
class State extends Model
{
    /**
     * @var string
     */
    protected $table = 'state';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','is_active'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function city(){
        return $this->hasMany('App\City');
    }
}