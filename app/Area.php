<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Area
 * @package App
 */
class Area extends Model
{
    /**
     * @var string
     */
    protected $table = 'area';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id','name','is_active','region'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function apply(){
        return $this->hasMany('App\Apply');
    }
}
