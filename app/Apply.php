<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Apply
 * @package App
 */
class Apply extends Model
{
    /**
     * @var string
     */
    protected $table = 'apply';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'user_id','area_id','phone', 'advice_option_ids', 'advice_ids', 'mobile','address','context','lat','lon','business_days','holidays','off_price','price','is_payment','payment_type'
    ];

    protected $casts = [
          'advice_option_ids' => 'array',
          'advice_ids' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function report()
    {
        return $this->hasOne('App\Report');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo('App\Area');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function building_advice_price()
    {
        return $this->belongsToMany('App\BuildingAdvicePrice', 'apply_building_advice_price', 'apply_id', 'building_advice_price_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function expert()
    {
        return $this->belongsToMany('App\User', 'apply_building_advice_price', 'apply_id', 'user_id')->withTimestamps();
    }

    public function transaction()
    {
        return $this->hasMany('App\Transaction');
    }
}
