<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MenuItem
 * @package App
 */
class MenuItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'menu_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'menu_id','title','link','position','is_active','parent','images'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu(){
        return $this->belongsTo('App\Menu');
    }

    /**
     * @return $this|null
     */
    public function child() {
        if($this->id == 0)
            return null;
        return $this::where('parent',$this->id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model|null|static|static[]
     */
    public function parent() {
        if($this->parent == 0)
            return null;
        return $this::find($this->parent);
    }

    /**
     * @return array
     */
    public function parents() {
        if(!$this->find($this->id)->parent) {
            return [$this->id];
        } else {
            return array_merge([$this->id],$this->parents($this->find($this->id)->parent));
        }
    }

    /**
     * @param $value
     */
    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getImagesAttribute($value)
    {
        return is_array($value)?$value:json_decode($value,true);
    }

}
