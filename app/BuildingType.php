<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BuildingType
 * @package App
 */
class BuildingType extends Model
{
    /**
     * @var string
     */
    protected $table = 'building_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','parent','images','position','is_active'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function price(){
        return $this->hasMany('App\BuildingTypePrice');
    }

    /**
     * @param $value
     */
    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getImagesAttribute($value)
    {
        return is_array($value)?$value:json_decode($value,true);
    }

    /**
     * @return mixed
     */
    public function child() {
        return $this::where('parent',$this->id);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model|null|static|static[]
     */
    public function parent() {
        if($this->parent == 0)
            return null;
        return $this::find($this->parent);
    }

    /**
     * @return array
     */
    public function parents() {
        if(!$this->find($this->id)->parent) {
            return [$this->id];
        } else {
            return array_merge([$this->id],$this->parents($this->find($this->id)->parent));
        }
    }
}
