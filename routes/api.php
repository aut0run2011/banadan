<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::group(['as'=>'admin.','prefix' => 'admin', 'namespace' => 'Admin' ], function()
{
  Route::get('/page/test', function() {
    return App\User::first();
  });
  Route::resource('/user', 'UserController',['except' => ['destroy']]);
  Route::put('/user/status/{user}',['as' => 'user.status', 'uses' => 'UserController@status']);
  Route::resource('/advice_option', 'AdviceOptionController',['except' => ['destroy','create','update']]);
  Route::post('/advice_option/update',['as' => 'advice_option.update', 'uses' => 'AdviceOptionController@update']);
  Route::resource('/apply', 'ApplyController',['except' => ['destroy','create','store']]);
  Route::resource('/area', 'AreaController',['except' => ['destroy','create']]);
  Route::resource('/city', 'CityController',['except' => ['destroy','create']]);
  Route::resource('/state', 'StateController',['except' => ['destroy','create']]);
  Route::resource('/page', 'PageController',['except' => ['destroy','create']]);
  Route::put('/page/status/{page}',['as' => 'page.status', 'uses' => 'PageController@status']);
  Route::resource('/menu', 'MenuController',['except' => ['destroy','create']]);
  Route::put('/menu/status/{menu}',['as' => 'menu.status', 'uses' => 'MenuController@status']);
  Route::put('/menu_item/status/{id}',['as' => 'menu_item.status', 'uses' => 'MenuItemController@status']);
  Route::resource('/menu_item', 'MenuItemController',['except' => ['destroy','create']]);
  Route::post('/file',['as' => 'file.store', 'uses' => 'AttachmentController@store']);
  Route::post('/file/delete',['as' => 'file.destroy', 'uses' => 'AttachmentController@destroy']);

  Route::resource('/building_type', 'BuildingTypeController',['except' => ['destroy','create','edit','update']]);
  Route::put('/building_type/status/{buildingType}',['as' => 'buildingType.status', 'uses' => 'BuildingTypeController@status']);
  Route::post('/building_type/update',['as' => 'buildingType.update', 'uses' => 'BuildingTypeController@update']);
  Route::resource('/building_advice_price', 'BuildingAdvicePriceController',['except' => ['destroy','edit']]);
  Route::put('/building_advice_price/status/{buildingAdvicePrice}',['as' => 'buildingAdvicePrice.status', 'uses' => 'BuildingAdvicePriceController@status']);

  Route::get('/list/state', ['as' => 'home.state', 'uses' => 'StateController@state']);
  Route::get('/list/city/{state_id}', ['as' => 'home.city', 'uses' => 'StateController@city']);
  Route::get('/list/area/{city_id}', ['as' => 'home.city', 'uses' => 'StateController@area']);

/*  Route::post('state/post', 'PublicController@state_post');*/



});
Route::group(['as'=>'expert.','prefix' => 'expert', 'namespace' => 'Expert' ], function()
{

  Route::resource('/list', 'ListController',['only' => ['show']]);

  Route::resource('/user', 'UserController',['except' => ['destroy']]);

  Route::get('/report/user/{user}',['as' => 'report.user', 'uses' => 'ReportController@showUser']);
  Route::resource('/report', 'ReportController',['except' => ['destroy', 'create']]);



});
