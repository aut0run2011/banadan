<?php

Route::group(['as'=>'public.','prefix' => '/'], function() {
  Route::get('test', 'PublicController@test');
    Route::get('page/{slug?}', ['as' => 'page.index', 'uses' => 'PublicController@page']);
    Route::post('user/login', ['as' => 'user.login.auth', 'uses' => 'UserController@auth']);
    Route::get('', ['as' => 'home.index', 'uses' => 'PublicController@home']);
    //Route::get('test_sms', ['as' => 'home.index', 'uses' => 'PublicController@sms_test']);
    Route::get('state', ['as' => 'home.state', 'uses' => 'PublicController@state']);
    Route::get('city/{state_id}', ['as' => 'home.city', 'uses' => 'PublicController@city']);
    Route::get('area/{city_id}', ['as' => 'home.city', 'uses' => 'PublicController@area']);
    Route::get('apply/create', ['as' => 'apply.create', 'uses' => 'ApplyController@create']);
    Route::post('apply', ['as' => 'apply.store', 'uses' => 'ApplyController@store']);
    Route::post('price/check', ['as' => 'apply.price_check', 'uses' => 'ApplyController@price_check']);
    Route::get('logout', ['as' => 'user.logout', 'uses' => 'UserController@logout']);
    Route::get('pay/{id}', ['as' => 'payment.pay', 'uses' => 'PaymentController@pay']);
    Route::get('pay/verify/{hash_str?}', ['as' => 'payment.verify', 'uses' => 'PaymentController@verify']);

});
