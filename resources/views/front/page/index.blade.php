@extends('front.master')
@section('content')
<main id="top">




        <section id="five" class="dark-back" style="height: 100% !IMPORTANT;">
            <div class="pages box-txt">

            <div class="page-wrapper">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="page-content">
                      <br><br><br><br><br><br><br>
                      <h2 class="txt-center text-title">{{$page->title}}</h2>
                      <hr>
                      {!! $page->content !!}
                      <p class="font-16" style="padding: 0 60px;">

                        آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                                               آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.
                                               آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                                               آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.
                                               آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                                               آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.</p>
                                </div>
                </div>
                <div class="col-md-1"></div>
                <div class="clearfix"></div>
             </div>
            </div>
        </section>
    </main>


<!-- Modal login-->


<!-- payment success modal -->



<!-- payment failed modal -->

@endsection

@section('scripts')
    <script type="text/javascript">



        $(document).ready(function() {
           window.options = { now: "12:35",  twentyFour: true};
           window.timepickers = $('.timepicker').wickedpicker(window.options);
            window.elements =[];
            window.elmpadding ='';
            window.elmmar = 0;

            //get state list
            $.get("{{url('state')}}",
                function(data, status){
                    window.state = data.state;

                    for(var i = 0; i < window.state.length; i++) {
                        $('#state').append('<option value="' + window.state[i].id + '">' +
                            window.state[i].name + '</option>');
                    }

                });


            // get apply form info
            $.get("{{url('apply/create')}}",
                function(data, status){
                    window.buildingType = data.buildingType;
                    window.advice = data.advice;




                    function checkBuilding(value) {
                        for(var i = 0; i < value.length; i++) {
                            if(value[i].children.length > 0){

                                $('#buildingType').append('<option value="' + value[i].id + '">'+window.elmpadding +
                                    ' ' + value[i].name + ' </option>');

                                window.elmpadding = window.elmpadding.concat('&nbsp;&nbsp;');

                                checkBuilding(value[i].children);
                            }else{

                                $('#buildingType').append('<option value="' + value[i].id + '">'+window.elmpadding +
                                    ' ' + value[i].name + ' </option>');
                                window.elmpadding = '';
                            }
                        }

                    }

                    function checkAdvice(value) {
                        for(var i = 0; i < value.length; i++) {

                            if(value[i].children.length > 0){
                                $('#advice').append('<option value="' + value[i].id + '">' +
                                    value[i].name + ' </option>');

                                checkAdvice(value[i].children);
                            }else{

                                $('#advice').append('<option value="' + value[i].id + '">' +
                                    value[i].name + ' </option>');
                            }
                        }

                    }


                    function removeInputs(value) {

                        for(var i = 0; i < value.length; i++) {
                            if(value[i].children.length !== 0){
                                removeInputs(value[i].children);
                            }else {
                                //make input visible
                                window.elmmar = 0;
                                for(var j = 0; j < window.elements.length; j++) {
                                    if (window.elements[j].val() == value[i].id) {
                                        window.elements[j].css('visibility','visible');
                                        window.elements[j].css('width','13px');
                                        window.elements[j].prop({
                                            disabled: false
                                        });
                                        if(value[i].parent === 0){
                                            window.elements[j].css('margin-right', '5px');
                                        }
                                    }

                                }
                            }


                        }
                    }



                    setTimeout(function () {
                        checkAdvice(window.advice);
                        checkBuilding(window.buildingType);

                        $('#advice').multipleSelect()
                        .next().find('.ms-drop li label').each(function () {
                            $(this).prop({
                                disabled: true
                            });
                            var inputElm = $(this).find( "input" );
                            inputElm.css('margin-right', window.elmmar+'px');
                            window.elements.push(inputElm);
                            window.elmmar += 7;
                        });
                        $('.ms-select-all').remove();

                        removeInputs(window.advice);
                    },100);


                });

            $('#city').attr('disabled',true);
            $('#area').attr('disabled',true);

            // show payment list
            var paymentList = [
                {value:'online', name:'آنلاین'},
                {value:'place', name:'پرداخت در محل'},
                {value:'report', name:'گزارش قیمت'}
            ];
            for(var i = 0; i < paymentList.length; i++) {
                $('#payment_type').append('<option value="' +paymentList[i].value + '">' +
                    paymentList[i].name + '</option>');
            }
        });

        function submit( ) {
            var area_id = $( "#area" ).val(),
                address = $("#address").val(),
                buildingType_id = $( "#buildingType" ).val(),
                advice_ids = $( "#advice" ).val(),
                payment_type = $( "#payment_type" ).val(),
                context = $( "#context" ).val(),
                mobile = $( "#mobile" ).val(),
                phone = $( "#phone" ).val(),
                email = $( "#email" ).val(),
                meter = $( "#meter" ).val(),
                holidays= window.timepickers.wickedpicker('time', 0)+' - '+ window.timepickers.wickedpicker('time', 1),
                business_days= window.timepickers.wickedpicker('time', 2)+' - '+ window.timepickers.wickedpicker('time', 3),
                captcha = grecaptcha.getResponse(),
                url = "{{url('apply')}}";


            // Stop form from submitting normally
//            event.preventDefault();


            $.ajax({
                url: url,
                type: 'POST',
                data: { area_id: area_id,
                    address:address,
                    buildingType_id:buildingType_id,
                    advice_ids:advice_ids,
                    payment_type:payment_type,
                    context:context,
                    mobile:mobile,
                    phone:phone,
                    email:email,
                    meter:meter,
                    holidays:holidays,
                    business_days:business_days,
                    lat:0,
                    lon:0,
                    g_recaptcha_response:captcha,
                    _token: "{{ csrf_token() }}"

                },
                success: function(data) {
                    if(data.message){
                        if(Object.keys(data.message)[0] === 'password') {
                            $("#login").modal();
                        }
                    }else if(payment_type === 'online'){
                        location.replace("{{url('pay')}}/" + data.data.id);
                    }else{
                        toastr.success('درخواست شما با موفقیت ارسال شد.');
                    }
                },
                error:function (error) {

                        $('#stateErorr ul').empty();
                        $('#cityErorr ul').empty();
                        $('#areaErorr ul').empty();
                        $('#addressErorr ul').empty();
                        $('#mobileErorr ul').empty();
                        $('#phoneErorr ul').empty();
                        $('#emailErorr ul').empty();
                        $('#adviceErorr ul').empty();
                        $('#meterErorr ul').empty();

                        if(error.responseJSON.status === 'error'){
                           if(error.responseJSON.message.area_id){
                               $.each(error.responseJSON.message.area_id, function(key,value) {
                                   $('#stateErorr ul').append(
                                       '<li>این فیلد الزامی است</li>'
                                   )
                               });
                               $.each(error.responseJSON.message.area_id, function(key,value) {
                                   $('#cityErorr ul').append(
                                       '<li> این فیلد الزامی است</li>'
                                   )
                               });
                               $.each(error.responseJSON.message.area_id, function(key,value) {
                                   $('#areaErorr ul').append(
                                       '<li> ' + value + '</li>'
                                   )
                               });
                           }
                            if(error.responseJSON.message.address) {
                                $.each(error.responseJSON.message.address, function (key, value) {
                                    $('#addressErorr ul').append(
                                        '<li>' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.mobile) {
                                $.each(error.responseJSON.message.mobile, function (key, value) {
                                    $('#mobileErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.phone) {
                                $.each(error.responseJSON.message.phone, function (key, value) {
                                    $('#phoneErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.email) {
                                $.each(error.responseJSON.message.email, function (key, value) {
                                    $('#emailErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.advice_ids) {
                                $.each(error.responseJSON.message.advice_ids, function (key, value) {
                                    $('#adviceErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.meter) {
                                $.each(error.responseJSON.message.meter, function (key, value) {
                                    $('#meterErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }

                        }

                }

            });
        }

        function getCityByStateId() {
            $('#city')
                .find('option')
                .remove()
                .end();
            var getstateId = $('#state').find(":selected").val();
            $.get("{{url('city')}}/"+getstateId,
                function(data, status){
                    window.city = data.city;

                    if(window.city.length === 0){
                        $('#city').attr('disabled',true);
                    }else {
                        $('#city').attr('disabled',false);
                    }

                    for(var i = 0; i < window.city.length; i++){
                        $('#city').append('<option value="' + window.city[i].id + '">' +
                            window.city[i].name +'</option>');
                    }

                });
        }

        function getAreaByCityId() {
            $('#area')
                .find('option')
                .remove()
                .end();
            var getareaId = $('#city').find(":selected").val();
            $.get("{{url('area')}}/"+getareaId,
                function(data, status){
                    window.area = data.area;

                    if(window.area.length === 0){
                        $('#area').attr('disabled',true);
                    }else {
                        $('#area').attr('disabled',false);
                    }

                    for(var i = 0; i < window.area.length; i++){
                        $('#area').append('<option value="' + window.area[i].id + '">' +
                            window.area[i].name +'</option>');
                    }

                });
        }

        function loginAndSubmit() {

                var area_id = $( "#area" ).val(),
                    address = $("#address").val(),
                    buildingType_id = $( "#buildingType" ).val(),
                    advice_ids = $( "#advice" ).val(),
                    payment_type = $( "#payment_type" ).val(),
                    context = $( "#context" ).val(),
                    mobile = $( "#mobile" ).val(),
                    phone = $( "#phone" ).val(),
                    email = $( "#email" ).val(),
                    meter = $( "#meter" ).val(),
                    holidays= window.timepickers.wickedpicker('time', 0)+' - '+ window.timepickers.wickedpicker('time', 1),
                    business_days= window.timepickers.wickedpicker('time', 2)+' - '+ window.timepickers.wickedpicker('time', 3),
                    password = $( "#password" ).val(),
                    captcha = grecaptcha.getResponse(),
                    url = "{{url('apply')}}";


                $.ajax({
                    url: url,
                    type: 'POST',
                    data: { area_id: area_id,
                        address:address,
                        buildingType_id:buildingType_id,
                        advice_ids:advice_ids,
                        payment_type:payment_type,
                        context:context,
                        mobile:mobile,
                        phone:phone,
                        email:email,
                        meter:meter,
                        holidays:holidays,
                        business_days:business_days,
                        lat:0,
                        lon:0,
                        password:password,
                        g_recaptcha_response:captcha,
                        _token: "{{ csrf_token() }}"

                    },
                    success: function(data) {
                        if(payment_type === 'online'){
                            location.replace("{{url('pay')}}/" + data.data.id);
                        }else{
                            toastr.success('درخواست شما با موفقیت ارسال شد.');
                            $('#login').modal('toggle');
                        }
                    },
                    error:function (error) {
                    }

                });
        }

        function priceCheck() {

            var buildingType_id = $( "#buildingType" ).val(),
                advice_ids = $( "#advice" ).val(),
                meter = $( "#meter" ).val(),
                captcha = grecaptcha.getResponse(),
                url = "{{url('price/check')}}";

            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    buildingType_id:buildingType_id,
                    advice_ids:advice_ids,
                    meter:meter,
                    g_recaptcha_response:captcha,
                    _token: "{{ csrf_token() }}"

                },
                success: function(data) {
                    $('#showPrive span').remove();
                    $('#showPrive').append(
                        '<span> هزینه محاسبه شده ‌<strong style="color: #00ABBE">' + data.data.price + '</strong> تومان میباشد و  </span> ',
                        '<span> هزینه با احتساب تخفیف  <strong style="color: #00ABBE">'+ data.data.off_price +'</strong> تومان میباشد. </span> '
                    )
                },
                error:function (error) {
                    $('#adviceErorr ul').empty();
                    $('#meterErorr ul').empty();

                    if(error.responseJSON.status === 'error'){
                        $.each(error.responseJSON.message.advice_ids, function(key,value) {
                            $('#adviceErorr ul').append(
                                '<li> - ' + value + '</li>'
                            )
                        });
                        $.each(error.responseJSON.message.meter, function(key,value) {
                            $('#meterErorr ul').append(
                                '<li> - ' + value + ' </li>'
                            )
                        });

                    }
                }

            });
        }
    </script>
@append
