<!DOCTYPE html>
<html lang="fa" dir="rtl">
    <head>
        @include('front.master.head')
        @yield('head')
    </head>
    <body>
        @include('front.master.header')

        @yield('content')
        @include('front.master.footer')
    </body>
    @include('front.master.scripts')
    @yield('scripts')
</html>