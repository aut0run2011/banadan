@extends('front.master')
@section('content')
<main id="top">
        <section id="one">
            <div class="col-md-6 col-xs-12 right box-txt pa0">
                <div class="pagination transparent hidden-xs hidden-sm">
                    <ul>
                        <li class="prev" style="height: 0;">
                           <a>
                              <i class="scroll-up"></i>
                              <p style="top: 25vh;"></p>
                           </a>
                        </li>
                        <li class="next" style="height: 100vh;">
                           <a data-easing="linear" data-scroll href="#two">
                              <p style="bottom: 50%; display:block;">مشاوره خرید و فروش</p>
                              <i class="scroll-down"></i>
                           </a>
                        </li>
                    </ul>
                </div>
                <div class="text-section col-md-offset-2">
                    <div class="content">
                        <h2 class="text-title font-24">مشاوره خرید و فروش ملک شما</h2>
                        <p class="font-16">آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                        <br>
                        <span>آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.</span>
                        </p>
                        <div class="button-wrapper">
                        <button class="blue-button">ادامه مطلب</button>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 left half-box-img pa0"
                   style="background-image:url({{url('assets/img/left.jpg')}});" >
                <img src="{{url('assets/img/left.jpg')}}" style="display: none">
            </div>
        </section>
        <section id="two">
                    <div class="col-md-6 col-xs-12 right box-txt pa0">
                        <div class="pagination hidden-xs hidden-sm">
                            <ul>
                                <li class="prev">
                                   <a data-easing="linear" data-scroll href="#one">
                                      <i class="scroll-up"></i>
                                      <p style="top: 25vh;">قبلی</p>
                                   </a>
                                </li>
                                <li class="next">
                                   <a data-easing="linear" data-scroll href="#three">
                                      <p style="bottom: 30%; display:block;">بعدی</p>
                                      <i class="scroll-down"></i>
                                   </a>
                                </li>
                            </ul>
                        </div>
                        <div class="text-section col-md-offset-2">
                            <div class="content">
                                <h2 class="text-title font-24">با متخصصان این صنعت آشنا شوید</h2>
                                <p class="font-16">آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                                <br>
                                <span>آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.</span>
                                </p>
                                <div class="button-wrapper">
                                <button class="blue-button">ادامه مطلب</button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 left half-box-img pa0"
                         style="background-image:url({{url('assets/img/lef2.jpg')}});">
                        <img src="{{url('assets/img/lef2.jpg')}}" style="display: none">
                    </div>
        </section>
        <section id="three" class="back-image" style="background-image: url('assets/img/back.jpg'); height: 115vh !IMPORTANT;">
        <div class="over"></div>
            <div class="pages box-txt">
              <div class="pagination hidden-xs hidden-sm">
                  <ul>
                      <li class="prev">
                         <a data-easing="linear" data-scroll href="#two">
                                                   <i class="scroll-up"></i>
                                                   <p style="top: 25vh;">قبلی</p>
                                                </a>
                      </li>
                      <li class="next">
                         <a data-easing="linear" data-scroll href="#four">
                                                  <p style="bottom: 30%; display:block;">بعدی</p>
                                                  <i class="scroll-down"></i>
                                                </a>
                      </li>
                  </ul>
              </div>
              <div class="page-wrapper">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                      <div class="page-content" >
                          <h2 class="request-title">کارشناس خود را بیابید</h2>
                          <div>

                                <div class="request-form" name="expertRequest" id="expertRequest">
                                    <div style="border-bottom: 3px solid #00ABBE;margin-bottom: 20px;padding-bottom: 20px">
                                        <div class="col-md-4 filed-box">
                                            <label class="font-14">نوع مشاوره</label>
                                            <select multiple="multiple" name="advice" class="" id="advice"
                                            >
                                            </select>
                                            <div class="font-12 errorItem" id="adviceErorr">
                                                <ul></ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4 filed-box">
                                            <label class="font-14">نوع ملک</label>
                                            <select  name="buildingType"
                                                     class="form-control input-style" id="buildingType">
                                            </select>
                                            <div class="font-12 errorItem" id="buildingTypeErorr">
                                                <ul></ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4 filed-box">
                                            <label class="font-14">متراژ</label>
                                            <input style="text-align: left;direction: ltr"
                                                   id="meter" name="meter" type="text" class="input-style form-control">
                                            <div class="font-12">برای متراژ بالای 1000 به صورت حضوری تماس حاصل بگیرید.</div>
                                            <div class="font-12 errorItem" id="meterErorr">
                                                <ul></ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="showPrive"></div>
                                        </div>
                                        <div class="button-wrapper col-md-6">
                                            <button onclick="priceCheck()" class="blue-button font-16">محاسبه قیمت</button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div>
                                      <div class="alert alert-info">
                                        <strong>توجه!</strong> تکمیل و ارسال درخواست کارشناسی به منزله‌ی قبول <a href="/page/rules">قوانین وب‌سایت</a> می‌باشد.
                                      </div>
                                    <div class="col-md-3 filed-box">
                                        <label for="state">استان</label>
                                        <select onclick="getCityByStateId()" name="state"
                                                class="form-control input-style" id="state">
                                        </select>
                                        <div class="font-12 errorItem" id="stateErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 filed-box">
                                        <label for="city">شهر</label>
                                        <select onclick="getAreaByCityId()" name="city"
                                                class="form-control input-style" id="city">
                                        </select>
                                        <div class="font-12 errorItem" id="cityErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 filed-box">
                                        <label for="area">ناحیه</label>
                                        <select  name="area" class="form-control input-style" id="area">
                                        </select>
                                        <div class="font-12 errorItem" id="areaErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 filed-box">
                                        <label class="font-14">آدرس</label>
                                        <input placeholder="خیابان..."
                                               id="address" name="address" type="text" class="input-style form-control">
                                        <div class="font-12 errorItem" id="addressErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 filed-box">
                                        <label class="font-14">شماره تلفن همراه</label>
                                        <input style="text-align: left;direction: ltr"
                                               placeholder="09*********" id="mobile" name="mobile"
                                               type="text" class="input-style form-control">
                                        <div class="font-12 errorItem" id="mobileErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 filed-box">
                                        <label class="font-14">شماره تلفن ثابت</label>
                                        <input style="text-align: left;direction: ltr"
                                               placeholder="02*********" id="phone" name="phone"
                                               type="text" class="input-style form-control">
                                        <div class="font-12 errorItem" id="phoneErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 filed-box">
                                        <label class="font-14">ایمیل</label>
                                        <input style="text-align: left;direction: ltr"
                                               placeholder="Example@gmail.com"
                                               id="email" name="email" type="email" class="input-style form-control">
                                        <div class="font-12 errorItem" id="emailErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 filed-box">
                                        <label for="payment_type">نوع پرداخت</label>
                                        <select  name="payment_type" class="form-control input-style" id="payment_type">
                                        </select>
                                        <div class="font-12 errorItem" id="payment_typeErorr">
                                            <ul></ul>
                                        </div>
                                    </div>

                                    <div class="col-md-3 date filed-box">
                                        <label class="font-14">روزهای تعطیل</label>
                                        <span>از</span>
                                        <input id="holidays-from" name="holidays-from" type="text"
                                               class="input-style form-control date-margin timepicker">
                                        <span>تا</span>
                                        <input id="holidays-to" name="holidays-to" type="text"
                                               class="input-style form-control timepicker">
                                        <div class="font-12 errorItem" id="holidaysErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 date filed-box">
                                        <label class="font-14">روزهای کاری</label>
                                        <span>از</span>
                                        <input id="business_days-from" name="business_days-from" type="text"
                                               class="input-style form-control date-margin timepicker">
                                        <span>تا</span>
                                        <input id="business_days-to" name="business_days-to" type="text"
                                               class="input-style form-control timepicker">
                                        <div class="font-12 errorItem" id="business_daysErorr">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6 filed-box">
                                        <label class="font-14">توضیحات</label>
                                        <textarea id="context" class="input-style form-control"></textarea>
                                    </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="button-wrapper col-md-12">
                                        <div style="position: absolute">{!!  Recaptcha::render()!!}</div>
                                        <button id="sendForm" onclick="submit()" class="blue-button font-16">درخواست کارشناس</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-1"></div>
                  <div class="clearfix"></div>
              </div>
            </div>
        </section>
        <section id="four">
          <div class="col-md-6 col-xs-12 right box-txt pa0">
              <div class="pagination hidden-xs hidden-sm">
                  <ul>
                     <li class="prev">
                        <a data-easing="linear" data-scroll href="#three">
                           <i class="scroll-up"></i>
                           <p style="top: 25vh;">قبلی</p>
                        </a>
                     </li>
                     <li class="next">
                        <a data-easing="linear" data-scroll href="#five">
                          <p style="bottom: 30%; display:block;">بعدی</p>
                          <i class="scroll-down"></i>
                        </a>
                     </li>
                  </ul>
              </div>
              <div class="text-section col-md-offset-2">
                  <div class="content">
                     <h2 class="text-title font-24">با متخصصان این صنعت آشنا شوید</h2>
                        <p class="font-16">آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                        <br>
                        <span>آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.</span>
                        </p>
                        <div class="button-wrapper">
                            <button class="blue-button">ادامه مطلب</button>
                        </div>
                  </div>
               </div>
              </div>
          <div class="col-md-6 col-xs-12 left half-box-img pa0"
               style="background-image:url({{url('assets/img/left3.jpg')}});">
              <img src="{{url('assets/img/left3.jpg')}}" style="display: none">
          </div>
         </section>
        <section id="five" class="dark-back">
            <div class="pages box-txt">
            <div class="pagination hidden-xs hidden-sm">
                <ul>
                    <li class="prev">
                       <a data-easing="linear" data-scroll href="#four">
                          <i class="scroll-up"></i>
                          <p style="top: 25vh;">قبلی</p>
                       </a>
                    </li>
                    <li class="next">
                        <a data-easing="linear" data-scroll href="#footer">
                           <p style="bottom: 30%; display:block;">بعدی</p>
                           <i class="scroll-down"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="page-wrapper">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="page-content">
                      <h2 class="txt-center text-title">درباره ما</h2>
                      <p class="font-16" style="padding: 0 60px;">آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                                               آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.
                                               آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                                               آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.
                                               آشنایی با متخصصان صنعت ساختمان در بالا رفتن کیفیت ساخت و ساز موثر میباشد.
                                               آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.</p>
                                </div>
                </div>
                <div class="col-md-1"></div>
                <div class="clearfix"></div>
             </div>
            </div>
        </section>
    </main>


<!-- Modal login-->
<div id="login" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title font-16">ورود به سایت</h4>
            </div>
            <div class="modal-body">
                    <p class="col-md-8 col-md-offset-2">کاربر گرامی،
                        <br> لطفا برای  ثبت درخواست خود رمز عبور خود را وارد کنید تا وارد سایت شوید. </p>
                    <div class="col-md-8 col-md-offset-2">
                        <div>
                            <label>رمز عبور</label>
                            <input id="password" name="password" type="password" class="input-style form-control">
                            <div class="font-12 errorItem" id="editnameErorr">
                                <ul></ul>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-8 col-md-offset-2 button-wrapper">
                        <button class="cancel" data-dismiss="modal">انصراف</button>
                        <button onclick="loginAndSubmit()" class="blue-button">ورود و ثبت درخواست</button>
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- payment success modal -->
<div id="paymentSuccess" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title font-16">پرداخت</h4>
            </div>
            <div class="modal-body">
                <p class="col-md-8 col-md-offset-2">کاربر گرامی،
                    <br>پرداخت شما با موفقیت ارسال شد. منتظر تماس همکاران ما باشید.</p>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>


<!-- payment failed modal -->
<div id="paymentFailed" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title font-16">پرداخت</h4>
            </div>
            <div class="modal-body">
                <p class="col-md-8 col-md-offset-2">کاربر گرامی،
                    <br>پرداخت شما با مشکل مواجه شد.</p>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">



        $(document).ready(function() {
           window.options = { now: "12:35",  twentyFour: true};
           window.timepickers = $('.timepicker').wickedpicker(window.options);
            window.elements =[];
            window.elmpadding ='';
            window.elmmar = 0;

            //get state list
            $.get("{{url('state')}}",
                function(data, status){
                    window.state = data.state;

                    for(var i = 0; i < window.state.length; i++) {
                        $('#state').append('<option value="' + window.state[i].id + '">' +
                            window.state[i].name + '</option>');
                    }

                });


            // get apply form info
            $.get("{{url('apply/create')}}",
                function(data, status){
                    window.buildingType = data.buildingType;
                    window.advice = data.advice;




                    function checkBuilding(value) {
                        for(var i = 0; i < value.length; i++) {
                            if(value[i].children.length > 0){

                                $('#buildingType').append('<option value="' + value[i].id + '">'+window.elmpadding +
                                    ' ' + value[i].name + ' </option>');

                                window.elmpadding = window.elmpadding.concat('&nbsp;&nbsp;');

                                checkBuilding(value[i].children);
                            }else{

                                $('#buildingType').append('<option value="' + value[i].id + '">'+window.elmpadding +
                                    ' ' + value[i].name + ' </option>');
                                window.elmpadding = '';
                            }
                        }

                    }

                    function checkAdvice(value) {
                        for(var i = 0; i < value.length; i++) {

                            if(value[i].children.length > 0){
                                $('#advice').append('<option value="' + value[i].id + '">' +
                                    value[i].name + ' </option>');

                                checkAdvice(value[i].children);
                            }else{

                                $('#advice').append('<option value="' + value[i].id + '">' +
                                    value[i].name + ' </option>');
                            }
                        }

                    }


                    function removeInputs(value) {

                        for(var i = 0; i < value.length; i++) {
                            if(value[i].children.length !== 0){
                                removeInputs(value[i].children);
                            }else {
                                //make input visible
                                window.elmmar = 0;
                                for(var j = 0; j < window.elements.length; j++) {
                                    if (window.elements[j].val() == value[i].id) {
                                        window.elements[j].css('visibility','visible');
                                        window.elements[j].css('width','13px');
                                        window.elements[j].prop({
                                            disabled: false
                                        });
                                        if(value[i].parent === 0){
                                            window.elements[j].css('margin-right', '5px');
                                        }
                                    }

                                }
                            }


                        }
                    }



                    setTimeout(function () {
                        checkAdvice(window.advice);
                        checkBuilding(window.buildingType);

                        $('#advice').multipleSelect()
                        .next().find('.ms-drop li label').each(function () {
                            $(this).prop({
                                disabled: true
                            });
                            var inputElm = $(this).find( "input" );
                            inputElm.css('margin-right', window.elmmar+'px');
                            window.elements.push(inputElm);
                            window.elmmar += 7;
                        });
                        $('.ms-select-all').remove();

                        removeInputs(window.advice);
                    },100);


                });

            $('#city').attr('disabled',true);
            $('#area').attr('disabled',true);

            // show payment list
            var paymentList = [
                {value:'online', name:'آنلاین'},
                {value:'place', name:'پرداخت در محل'},
                {value:'report', name:'گزارش قیمت'}
            ];
            for(var i = 0; i < paymentList.length; i++) {
                $('#payment_type').append('<option value="' +paymentList[i].value + '">' +
                    paymentList[i].name + '</option>');
            }
        });

        function submit( ) {
            var area_id = $( "#area" ).val(),
                address = $("#address").val(),
                buildingType_id = $( "#buildingType" ).val(),
                advice_ids = $( "#advice" ).val(),
                payment_type = $( "#payment_type" ).val(),
                context = $( "#context" ).val(),
                mobile = $( "#mobile" ).val(),
                phone = $( "#phone" ).val(),
                email = $( "#email" ).val(),
                meter = $( "#meter" ).val(),
                holidays= window.timepickers.wickedpicker('time', 0)+' - '+ window.timepickers.wickedpicker('time', 1),
                business_days= window.timepickers.wickedpicker('time', 2)+' - '+ window.timepickers.wickedpicker('time', 3),
                captcha = grecaptcha.getResponse(),
                url = "{{url('apply')}}";


            // Stop form from submitting normally
//            event.preventDefault();


            $.ajax({
                url: url,
                type: 'POST',
                data: { area_id: area_id,
                    address:address,
                    buildingType_id:buildingType_id,
                    advice_ids:advice_ids,
                    payment_type:payment_type,
                    context:context,
                    mobile:mobile,
                    phone:phone,
                    email:email,
                    meter:meter,
                    holidays:holidays,
                    business_days:business_days,
                    lat:0,
                    lon:0,
                    g_recaptcha_response:captcha,
                    _token: "{{ csrf_token() }}"

                },
                success: function(data) {
                    if(data.message){
                        if(Object.keys(data.message)[0] === 'password') {
                            $("#login").modal();
                        }
                    }else if(payment_type === 'online'){
                        location.replace("{{url('pay')}}/" + data.data.id);
                    }else{
                        toastr.success('درخواست شما با موفقیت ارسال شد.');
                    }
                },
                error:function (error) {

                        $('#stateErorr ul').empty();
                        $('#cityErorr ul').empty();
                        $('#areaErorr ul').empty();
                        $('#addressErorr ul').empty();
                        $('#mobileErorr ul').empty();
                        $('#phoneErorr ul').empty();
                        $('#emailErorr ul').empty();
                        $('#adviceErorr ul').empty();
                        $('#meterErorr ul').empty();

                        if(error.responseJSON.status === 'error'){
                           if(error.responseJSON.message.area_id){
                               $.each(error.responseJSON.message.area_id, function(key,value) {
                                   $('#stateErorr ul').append(
                                       '<li>این فیلد الزامی است</li>'
                                   )
                               });
                               $.each(error.responseJSON.message.area_id, function(key,value) {
                                   $('#cityErorr ul').append(
                                       '<li> این فیلد الزامی است</li>'
                                   )
                               });
                               $.each(error.responseJSON.message.area_id, function(key,value) {
                                   $('#areaErorr ul').append(
                                       '<li> ' + value + '</li>'
                                   )
                               });
                           }
                            if(error.responseJSON.message.address) {
                                $.each(error.responseJSON.message.address, function (key, value) {
                                    $('#addressErorr ul').append(
                                        '<li>' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.mobile) {
                                $.each(error.responseJSON.message.mobile, function (key, value) {
                                    $('#mobileErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.phone) {
                                $.each(error.responseJSON.message.phone, function (key, value) {
                                    $('#phoneErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.email) {
                                $.each(error.responseJSON.message.email, function (key, value) {
                                    $('#emailErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.advice_ids) {
                                $.each(error.responseJSON.message.advice_ids, function (key, value) {
                                    $('#adviceErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }
                            if(error.responseJSON.message.meter) {
                                $.each(error.responseJSON.message.meter, function (key, value) {
                                    $('#meterErorr ul').append(
                                        '<li> - ' + value + '</li>'
                                    )
                                });
                            }

                        }

                }

            });
        }

        function getCityByStateId() {
            $('#city')
                .find('option')
                .remove()
                .end();
            var getstateId = $('#state').find(":selected").val();
            $.get("{{url('city')}}/"+getstateId,
                function(data, status){
                    window.city = data.city;

                    if(window.city.length === 0){
                        $('#city').attr('disabled',true);
                    }else {
                        $('#city').attr('disabled',false);
                    }

                    for(var i = 0; i < window.city.length; i++){
                        $('#city').append('<option value="' + window.city[i].id + '">' +
                            window.city[i].name +'</option>');
                    }

                });
        }

        function getAreaByCityId() {
            $('#area')
                .find('option')
                .remove()
                .end();
            var getareaId = $('#city').find(":selected").val();
            $.get("{{url('area')}}/"+getareaId,
                function(data, status){
                    window.area = data.area;

                    if(window.area.length === 0){
                        $('#area').attr('disabled',true);
                    }else {
                        $('#area').attr('disabled',false);
                    }

                    for(var i = 0; i < window.area.length; i++){
                        $('#area').append('<option value="' + window.area[i].id + '">' +
                            window.area[i].name +'</option>');
                    }

                });
        }

        function loginAndSubmit() {

                var area_id = $( "#area" ).val(),
                    address = $("#address").val(),
                    buildingType_id = $( "#buildingType" ).val(),
                    advice_ids = $( "#advice" ).val(),
                    payment_type = $( "#payment_type" ).val(),
                    context = $( "#context" ).val(),
                    mobile = $( "#mobile" ).val(),
                    phone = $( "#phone" ).val(),
                    email = $( "#email" ).val(),
                    meter = $( "#meter" ).val(),
                    holidays= window.timepickers.wickedpicker('time', 0)+' - '+ window.timepickers.wickedpicker('time', 1),
                    business_days= window.timepickers.wickedpicker('time', 2)+' - '+ window.timepickers.wickedpicker('time', 3),
                    password = $( "#password" ).val(),
                    captcha = grecaptcha.getResponse(),
                    url = "{{url('apply')}}";


                $.ajax({
                    url: url,
                    type: 'POST',
                    data: { area_id: area_id,
                        address:address,
                        buildingType_id:buildingType_id,
                        advice_ids:advice_ids,
                        payment_type:payment_type,
                        context:context,
                        mobile:mobile,
                        phone:phone,
                        email:email,
                        meter:meter,
                        holidays:holidays,
                        business_days:business_days,
                        lat:0,
                        lon:0,
                        password:password,
                        g_recaptcha_response:captcha,
                        _token: "{{ csrf_token() }}"

                    },
                    success: function(data) {
                        if(payment_type === 'online'){
                            location.replace("{{url('pay')}}/" + data.data.id);
                        }else{
                            toastr.success('درخواست شما با موفقیت ارسال شد.');
                            $('#login').modal('toggle');
                        }
                    },
                    error:function (error) {
                    }

                });
        }

        function priceCheck() {

            var buildingType_id = $( "#buildingType" ).val(),
                advice_ids = $( "#advice" ).val(),
                meter = $( "#meter" ).val(),
                captcha = grecaptcha.getResponse(),
                url = "{{url('price/check')}}";

            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    buildingType_id:buildingType_id,
                    advice_ids:advice_ids,
                    meter:meter,
                    g_recaptcha_response:captcha,
                    _token: "{{ csrf_token() }}"

                },
                success: function(data) {
                    $('#showPrive span').remove();
                    $('#showPrive').append(
                        '<span> هزینه محاسبه شده ‌<strong style="color: #00ABBE">' + data.data.price + '</strong> تومان میباشد و  </span> ',
                        '<span> هزینه با احتساب تخفیف  <strong style="color: #00ABBE">'+ data.data.off_price +'</strong> تومان میباشد. </span> '
                    )
                },
                error:function (error) {
                    $('#adviceErorr ul').empty();
                    $('#meterErorr ul').empty();

                    if(error.responseJSON.status === 'error'){
                        $.each(error.responseJSON.message.advice_ids, function(key,value) {
                            $('#adviceErorr ul').append(
                                '<li> - ' + value + '</li>'
                            )
                        });
                        $.each(error.responseJSON.message.meter, function(key,value) {
                            $('#meterErorr ul').append(
                                '<li> - ' + value + ' </li>'
                            )
                        });

                    }
                }

            });
        }
    </script>
@append
