<footer id="footer">
    <div class="container footer-items">
        <div class="col-md-6 col-xs-12">
            <h4>درباره بنادان</h4>
            <p>آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.آشنایی با آموزش های تخصصی روش های اجرایی و ساخت در مدیریت بهتر پروژه ها امری ضروری است.</p>
        </div>
        <div class="col-md-2 col-xs-12 links">
            <ul>
                <li><a href="">درباره ما</a></li>
                <li><a href="">قوانین و مقررات</a></li>
                <li><a href="">پرسش های متداول</a></li>
                <li><a href="">ارتباط با ما</a></li>
            </ul>
        </div>
        <div class="col-md-4 col-xs-12 socials">
            <ul>
                <li><a href="https://t.me/banadan"><img src="{{url('assets/img/icon/social/if_--09_2644993.png')}}"/></a></li>
                <li><a href="https://www.instagram.com/banadan_com/"><img src="{{url('assets/img/icon/social/if_38-instagram_104466.png')}}"/></a></li>
                <li><a href="https://twitter.com/banadan_com"><img src="{{url('assets/img/icon/social/if_social-twitter_216402.png')}}"/></a></li>
                <li><a href="https://www.facebook.com/bana.dan.940"><img src="{{url('assets/img/icon/social/if_06-facebook_104498.png')}}"/></a></li>
                <li><a href="https://www.linkedin.com/in/banadan/"><img src="{{url('assets/img/icon/social/if_social-linkedin_216394.png')}}"/></a></li>
            </ul>
        </div>
    </div>
    <div class="copy-right font-16">تمامی حقوق مادی و معنوی سایت  محفوظ و متعلق به شرکت بنادان میباشد.</div>
</footer>
<div id="mySidenav" class="sidenav">
    <div class="container">
        <div class="col-md-12">
            <a class="navbar-brand" data-easing="linear" data-scroll href="#one" onclick="closeNav()"><img src="{{url('assets/img/logo-sefid.png')}}"></a>
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        </div>
        <div class="col-md-6 col-xs-12 pa0 menu-list">
            <ul class="">
                <li><a data-easing="linear" data-scroll href="#one" onclick="closeNav()">صفحه نخست</a></li>
                <li><a data-easing="linear" data-scroll href="#five" onclick="closeNav()">درباره ما</a></li>
                <li><a data-easing="linear" data-scroll href="#three" onclick="closeNav()">تماس با مشاورین</a></li>
                <li><a data-easing="linear" data-scroll href="#footer" onclick="closeNav()">تماس با ما</a></li>
            </ul>
        </div>
        <div class="col-md-6 hidden-xs hidden-sm pa0">
            <div class="menu-contact-info">
                    <p class="font-18">دفتر مرکزی: تهران، سهروردی شمالی، اندیشه دوم، پلاک 80 واحد 3</p>
                <p class="font-18">تلفن دفتر: 02188421130</p>
                <p class="font-18">تلفن همراه: 09127946920</p>
                <div class="socials">
                    <ul>
                        <li><a href="https://t.me/banadan"><img src="{{url('assets/img/icon/social/if_--09_2644993.png')}}"/></a></li>
                        <li><a href="https://www.instagram.com/banadan_com/"><img src="{{url('assets/img/icon/social/if_38-instagram_104466.png')}}"/></a></li>
                        <li><a href="https://twitter.com/banadan_com"><img src="{{url('assets/img/icon/social/if_social-twitter_216402.png')}}"/></a></li>
                        <li><a href="https://www.facebook.com/bana.dan.940"><img src="{{url('assets/img/icon/social/if_06-facebook_104498.png')}}"/></a></li>
                        <li><a href="https://www.linkedin.com/in/banadan/"><img src="{{url('assets/img/icon/social/if_social-linkedin_216394.png')}}"/></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
