<nav class="navbar navbar-fixed-top">
    <div style="padding: 0 8%">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" onclick="openNav()">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" data-easing="linear" data-scroll href="#one"><img src="{{url('assets/img/logo.png')}}"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <span class="main-menu" onclick="openNav()">&#9776;</span>
        </div>
    </div>
</nav>