<script src="{{url('assets/js/jquery.min.js')}}"></script>
<script src="{{url('assets/js/smooth-scroll.polyfills.js')}}"></script>
<script src="{{url('assets/js/bootstrap.js')}}"></script>
<script src="{{url('assets/js/wickedpicker.min.js')}}"></script>
<script src="{{url('assets/js/multiple-select.js')}}"></script>
<script src="{{url('assets/js/toastr.min.js')}}"></script>
<script>
    var linear = new SmoothScroll('[data-easing="linear"]', {easing: 'linear'});
    function openNav() {
        document.getElementById("mySidenav").style.width = "100%";
    }
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
    $(document).ready(function () {
        var url = window.location.href;
        if(url.indexOf('status=success') > -1){
            $("#paymentSuccess").modal();
        }else if(url.indexOf('status=') > -1 ){
            $("#paymentFailed").modal();
        }
    })
</script>