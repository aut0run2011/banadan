<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBuildingAdvicePriceChangeAdviceOptionToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_advice_price', function (Blueprint $table) {
            $table->unsignedInteger('advice_option_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_advice_price', function (Blueprint $table) {
            $table->unsignedInteger('advice_option_id')->change();
        });
    }
}
