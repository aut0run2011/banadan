<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('family')->nullable();
            $table->string('mobile')->unique();
            $table->string('email')->unique();
            $table->enum('gender',['male','female'])->default('male');
            $table->string('password');
            $table->enum('role',['user','expert','admin'])->default('user');
            $table->date('birthday')->nullable();
            $table->string('postal_code',12)->nullable();
            $table->string('national_code',12)->nullable();
            $table->enum('status',['active','inactive','disabled_by_admin'])->default('inactive');
            $table->string('images')->nullable();
            $table->string('token',64)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
