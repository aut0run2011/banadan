<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingAdvicePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_advice_price', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('building_type_id');
            $table->unsignedInteger('advice_option_id');
            $table->integer('meter_start');
            $table->integer('meter_end');
            $table->integer('meter_price');
            $table->integer('position');
            $table->boolean('is_active');
            $table->timestamps();

/*            $table->foreign('building_type_id')->references('id')->on('building_type')->onDelete('cascade');
            $table->foreign('advice_option_id')->references('id')->on('advice_option')->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('building_advice_price');
    }
}
