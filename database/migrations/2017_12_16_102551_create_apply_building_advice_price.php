<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplyBuildingAdvicePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apply_building_advice_price', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apply_id')->unsigned();
            $table->integer('building_advice_price_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('apply_id')
                ->references('id')->on('apply')
                ->onDelete('cascade');
            $table->foreign('building_advice_price_id')
                ->references('id')->on('building_advice_price')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apply_advice_option');
    }
}
