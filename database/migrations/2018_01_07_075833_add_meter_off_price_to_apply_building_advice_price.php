<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMeterOffPriceToApplyBuildingAdvicePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_advice_price',function(Blueprint $table){
            $table->integer('meter_off_price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_advice_price',function(Blueprint $table){
            $table->dropColumn(['meter_off_price']);
        });
    }
}
