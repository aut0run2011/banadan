<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBuildingAdvicePriceModifyAdviceOptionIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('building_advice_price', function (Blueprint $table) {
          $table->text('advice_option_ids')->after('advice_option_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building_advice_price', function (Blueprint $table) {
            $table->dropColumn('building_type_ids');
        });
    }
}
