<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction',function(Blueprint $table){
            $table->increments('id')->unique();
            $table->integer('apply_id')->unsigned();
            $table->string('ip',20);
            $table->string('user_agent',300);
            $table->string('status_pay')->nullable();
            $table->string('RefID')->nullable();
            $table->timestamps();

            $table->index(['status_pay','RefID']);

            $table->foreign('apply_id')
                ->references('id')->on('apply')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
