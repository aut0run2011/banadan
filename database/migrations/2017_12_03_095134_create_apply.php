<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apply', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->string('phone',15)->nullable();
            $table->string('mobile',15)->nullable();
            $table->string('holidays')->nullable();
            $table->string('business_days')->nullable();
            $table->double('lat')->nullable();
            $table->double('lon')->nullable();
            $table->text('address')->nullable();
            $table->text('context')->nullable();
            $table->integer('price')->default(0);
            $table->integer('off_price')->default(0);
            $table->boolean('is_payment')->default(0);
            $table->enum('payment_type',['online','place','report'])->nullable();;
            $table->timestamps();


            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade');

            $table->foreign('area_id')
                ->references('id')->on('area')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apply');
    }
}
